#!/bin/sh

# This script initilizes the tools injected into the workspace on startup.
#
# It uses the following environment variables
# $GL_TOOLS_DIR - directory where the tools are copied.
# $GL_EDITOR_LOG_LEVEL - log level for the server. defaults to "info".
# $GL_EDITOR_PORT - port on which the server is exposed. defaults to "60001".
# $GL_EDITOR_IGNORE_VERSION_MISMATCH - if set to true, the server works even when server and WebIDE versions do not match.
# $GL_EDITOR_ENABLE_MARKETPLACE - if set to true, set configuration to enable marketplace.
# $GL_EDITOR_EXTENSIONS_GALLERY_SERVICE_URL - service url for the extensions gallery.
# $GL_EDITOR_EXTENSIONS_GALLERY_ITEM_URL - item url for the extensions gallery.
# $GL_EDITOR_EXTENSIONS_GALLERY_RESOURCE_URL_TEMPLATE - resource url template for the extensions gallery.
# $GITLAB_WORKFLOW_TOKEN - a giltab personal access token to configure the gitlab vscode extension.
# $GITLAB_WORKFLOW_TOKEN_FILE - the contents of this file populate GITLAB_WORKFLOW_TOKEN if it is not set.

if [ -z "${GL_TOOLS_DIR}" ]; then
	echo "\$GL_TOOLS_DIR is not set"
	exit 1
fi

if [ -z "${GL_EDITOR_LOG_LEVEL}" ]; then
	GL_EDITOR_LOG_LEVEL="info"
fi

if [ -z "${GL_EDITOR_PORT}" ]; then
	GL_EDITOR_PORT="60001"
fi

if [ -z "${GL_EDITOR_EXTENSIONS_GALLERY_SERVICE_URL}" ]; then
	GL_EDITOR_EXTENSIONS_GALLERY_SERVICE_URL="https://open-vsx.org/vscode/gallery"
fi

if [ -z "${GL_EDITOR_EXTENSIONS_GALLERY_ITEM_URL}" ]; then
	GL_EDITOR_EXTENSIONS_GALLERY_ITEM_URL="https://open-vsx.org/vscode/item"
fi

if [ -z "${GL_EDITOR_EXTENSIONS_GALLERY_RESOURCE_URL_TEMPLATE}" ]; then
	GL_EDITOR_EXTENSIONS_GALLERY_RESOURCE_URL_TEMPLATE="https://open-vsx.org/api/{publisher}/{name}/{version}/file/{path}"
fi

PRODUCT_JSON_FILE="${GL_TOOLS_DIR}/vscode-reh-web/product.json"

if [ "$GL_EDITOR_IGNORE_VERSION_MISMATCH" = true ]; then
	# TODO: remove this section once issue is fixed - https://gitlab.com/gitlab-org/gitlab/-/issues/373669
	# remove "commit" key from product.json to avoid client-server mismatch
	# TODO: remove this once we are not worried about version mismatch
	# https://gitlab.com/gitlab-org/gitlab/-/issues/373669
	echo "Ignoring VS Code client-server version mismatch"
	sed -i.bak '/"commit"/d' "${PRODUCT_JSON_FILE}" && rm "${PRODUCT_JSON_FILE}.bak"
fi

if [ "$GL_EDITOR_ENABLE_MARKETPLACE" = true ]; then
	EXTENSIONS_GALLERY_KEY="{\\n\\t\"extensionsGallery\": {\\n\\t\\t\"serviceUrl\": \"${GL_EDITOR_EXTENSIONS_GALLERY_SERVICE_URL}\",\\n\\t\\t\"itemUrl\": \"${GL_EDITOR_EXTENSIONS_GALLERY_ITEM_URL}\",\\n\\t\\t\"resourceUrlTemplate\": \"${GL_EDITOR_EXTENSIONS_GALLERY_RESOURCE_URL_TEMPLATE}\"\\n\\t},"
	echo "Appending '${EXTENSIONS_GALLERY_KEY}' in '${PRODUCT_JSON_FILE}' at the beginning of the file"
	sed -i.bak "1s|.*|$EXTENSIONS_GALLERY_KEY|" "${PRODUCT_JSON_FILE}" && rm "${PRODUCT_JSON_FILE}.bak"
fi


echo "Contents of ${PRODUCT_JSON_FILE} are: "
cat "${PRODUCT_JSON_FILE}"
echo

echo "Starting server for the editor"
"${GL_TOOLS_DIR}/vscode-reh-web/bin/gitlab-webide-server" \
	--host "0.0.0.0" \
	--port "${GL_EDITOR_PORT}" \
	--log "${GL_EDITOR_LOG_LEVEL}" \
	--without-connection-token
