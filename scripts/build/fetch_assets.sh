#!/usr/bin/env bash

# This script fetches the assets specified in the config file
#
# It uses the following arguments
# $OS - OS e.g. linux.
# $PLATFORM - Platform e.g. amd64.
# $CONFIG_FILE - config file path e.g. config.yaml.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

OS=$1
PLATFORM=$2
CONFIG_FILE=$3

if [ -z "${OS}" ]; then
    echo "\$OS is not set"
    exit 1
fi

if [ -z "${PLATFORM}" ]; then
    echo "\$PLATFORM is not set"
    exit 1
fi

if [ -z "${CONFIG_FILE}" ]; then
    echo "\$CONFIG_FILE is not set"
    exit 1
fi

echo "Fetching assets for ${OS} ${PLATFORM}"

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"
BUILD_ASSETS_DIR="${ROOT_DIR}/.build/assets"
OS_PLATFORM_BUILD_ASSETS_DIR="${BUILD_ASSETS_DIR}/${OS}/${PLATFORM}"
mkdir -p "${OS_PLATFORM_BUILD_ASSETS_DIR}"

# builds the url to fetch asset for gitlab_web_ide_vscode_fork
build_url_for_gitlab_web_ide_vscode_fork() {
    repository=$1
    tag=$2
    file_name=$3
    os=$(echo "$4" | tr '[:upper:]' '[:lower:]')
    platform=$(echo "$5" | tr '[:upper:]' '[:lower:]')

    declare -A os_name_translator
    os_name_translator["linux"]="linux"
    os_name_translator["darwin"]="darwin"
    os_name_translator["windows"]="win32"

    declare -A platform_name_translator
    platform_name_translator["amd64"]="x64"
    platform_name_translator["arm64"]="arm64"

    os="${os_name_translator[$os]}"
    platform="${platform_name_translator[$platform]}"
    file_name="vscode-reh-web-${os}-${platform}-${tag}.tar.gz"
    package_meta=$(glab release view "${tag}" --repo "${repository}" | grep "package\s*$repository")
    package_url=$(echo "$package_meta" | grep -o 'https://[^ ]*')
    project_name=$(echo "$package_url" | cut -d'/' -f4,5)
    project_name_url_encoded=$(echo "$project_name" | jq -Rr @uri)
    package_id="${package_url##*/}"
    package_files=$(glab api "/projects/${project_name_url_encoded}/packages/${package_id}/package_files")
    file_id=$(echo "$package_files" | jq --arg file_name "${file_name}" '.[] | select(.file_name == $file_name) | .id')
    url="${package_url%packages*}package_files/${file_id}/download"
    printf '%s' "$url"
}

# returns the name of the file the asset should be saved as
# for gitlab_web_ide_vscode_fork
get_asset_dest_name_for_gitlab_web_ide_vscode_fork() {
    printf 'vscode-reh-web.tar.gz'
}

# builds the url to fetch asset for gitlab_vscode_extension
build_url_for_gitlab_vscode_extension() {
    repository=$1
    tag=$2
    file_name=$3

    package_url=$(glab release view "${tag}" --repo "${repository}" | grep -A 1 "ASSETS" | grep -Eo "https://.*${file_name}")
    printf '%s' "$package_url"
}

# returns the name of the file the asset should be saved as
# for gitlab_vscode_extension
get_asset_dest_name_for_gitlab_vscode_extension() {
    printf 'gitlab-workflow-vscode-extension.zip'
}

assets_count=$(yq eval '.assets | length - 1' "${CONFIG_FILE}")

for index in $(seq 0 "$assets_count"); do
    tag=$(yq eval ".assets[$index].tag" "${CONFIG_FILE}")
    file_name=$(yq eval ".assets[$index].file_name" "${CONFIG_FILE}")
    repository_url=$(yq eval ".assets[$index].repository" "${CONFIG_FILE}")
    id=$(yq eval ".assets[$index].id" "${CONFIG_FILE}")

    url_builder="build_url_for_$id"
    if ! type "$url_builder" &> /dev/null; then
        echo "$id has no url builder function configured"
        exit 1
    fi
    asset_download_url=$($url_builder "${repository_url}" "${tag}" "${file_name}" "${OS}" "${PLATFORM}")

    asset_dest_file_name_func="get_asset_dest_name_for_$id"
    if ! type "$asset_dest_file_name_func" &> /dev/null; then
        echo "$id has no get_asset_dest_name_for_$id function configured"
        exit 1
    fi
    asset_dest_file_name=$($asset_dest_file_name_func)

    printf '\nDownloading %s for %s %s at %s/%s\n' "${asset_download_url}" "${id}" "${tag}" "${OS_PLATFORM_BUILD_ASSETS_DIR}" "${asset_dest_file_name}"
    curl -L --output-dir "${OS_PLATFORM_BUILD_ASSETS_DIR}" -o "${asset_dest_file_name}" "${asset_download_url}"
done
