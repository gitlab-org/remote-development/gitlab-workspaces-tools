#!/usr/bin/env bash

# This script prepares for the docker build
#
# It uses the following arguments
# $OS - OS e.g. linux.
# $PLATFORM - Platform e.g. amd64.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

OS=$1
PLATFORM=$2

if [ -z "${OS}" ]; then
    echo "\$OS is not set"
    exit 1
fi

if [ -z "${PLATFORM}" ]; then
    echo "\$PLATFORM is not set"
    exit 1
fi

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"
BUILD_ASSETS_DIR="${ROOT_DIR}/.build/assets"
echo "Preparing docker build for ${OS} ${PLATFORM}"
OS_PLATFORM_BUILD_ASSETS_DIR="${BUILD_ASSETS_DIR}/${OS}/${PLATFORM}"

# Untar each tarball file for platform and OS specific docker build
for file in "$OS_PLATFORM_BUILD_ASSETS_DIR"/*; do
    if [ -f "$file" ] && [[ "$file" == *.tar.gz ]]; then
        filename_without_extension=$(basename "$file" ".tar.gz")
        mkdir -p "${OS_PLATFORM_BUILD_ASSETS_DIR}/${filename_without_extension}"
        tar -xvzf "${file}" -C "${OS_PLATFORM_BUILD_ASSETS_DIR}/${filename_without_extension}"
    elif [ -f "$file" ] && [[ "$file" == *.zip ]]; then
        filename_without_extension=$(basename "$file" ".zip")
        mkdir -p "${OS_PLATFORM_BUILD_ASSETS_DIR}/${filename_without_extension}"
        unzip "${file}" -d "${OS_PLATFORM_BUILD_ASSETS_DIR}/${filename_without_extension}"
    fi
done
