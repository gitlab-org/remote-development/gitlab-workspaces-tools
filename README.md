# gitlab-workspaces-tools

:warning:  Project has been moved to https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-tools :warning:

This repository contains code which is responsible for injecting tools inside a workspace on startup. The currently supported tools are listed in the [`config.yaml`](config.yaml) file.

## Config

The [`config.yaml`](config.yaml) file specifies the dependencies needed to build the image. Its schema is as follows:

``` yaml
assets: ## assets describes an array of dependencies, each with the following structure:
  - id: "{{ Identifies an asset. Used to define asset url builder function in scripts }}"
    repository: "{{ Git URL of the repository. Must be double-quoted }}"
    tag: "{{ Git tag of the release. Must be double quoted }}"
    file_name: "{{ Regex of the file to downloaded from the release assets }}"

image: "{{ Image name of the tools injector }}"
```

The `RENOVATE_BOT_REGEX` in the [`config validation script`](scripts/lint/validate_config.sh) validates the config so we do not break [gitlab-renovate-bot](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab-workspace-tools/gitlab-workspace-tools.config.js?ref_type=heads) dependency update suggestions. If a regex update is required, we must also update the gitlab-renovate-bot config regex and vice versa.

### Automation

Each asset in `config.yaml` is monitored by the [gitlab-renovate-bot](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab-workspace-tools/gitlab-workspace-tools.config.js?ref_type=heads). The bot opens MRs when a new tag is observed
for a dependency in the `config.yaml`. This notifies the team of any updates and gives them a chance to verify the builds with new dependencies before approving and merging the MR.

### Adding a new asset

- Add the asset in [`config.yaml`](config.yaml) following the structure of existing assets defiend.
- Define two functions in [`fetch_assets.sh`](/scripts/build/fetch_assets.sh). Assuming `id` is the `id` of the asset in the config file -
  - Function `build_url_for_${id}` prints the URL to fetch the asset from.
  - Function `get_asset_dest_name_for_${id}` prints the destination name of the file to download the asset to. This truncates the version name in the file, changes the extension of the file if required.

## Pipeline setup

- When a merge request is opened, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with tag `dev-${timestamp}`. This image can be used for test verification purposes. DO NOT use this image for production purposes since they will be deleted.
- When new changes are merged to master, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with tag `dev-${timestamp}`. This image can be used for test verification purposes. DO NOT use this image for production purposes since they will be deleted.
- When a new tag is created, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with the new tag. This image can be used for production purposes.
- [TODO](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-tools/-/issues/2): Cron job which is run weekly and deletes all images with `dev-${timestamp}` tags.

## Release process

To trigger the production build, a new tag must be manually pushed. The tag
should follow [semantic versioning](https://semver.org/#summary) rules. For example:

``` shell
TAG_NAME=0.1.0
# create an annotated tag
git tag -a ${TAG_NAME}
git push origin ${TAG_NAME}
```

This should then be followed up by making a release manually from the pushed tag. TODO: [automate release process](gitlab-org/remote-development/gitlab-workspaces-tools#3)

## Local development

### Install dependencies

- Install [asdf](https://asdf-vm.com/guide/getting-started.html) and [make](https://www.gnu.org/software/make/)

``` shell
 make install-prerequisites
```

### Docker build

```shell
OS=linux PLATFORM=amd64 ENV=local make docker-build-and-publish
```

This will yield a local image of with a tag of `local-${timestamp}`

### Troubleshooting

Individual steps in the build process can be run locally by triggering their make target. For example:

```shell
OS=linux PLATFORM=amd64 make fetch-assets
```

This could be benefical when debugging locally where you might need to asses or modify the output of each job.
