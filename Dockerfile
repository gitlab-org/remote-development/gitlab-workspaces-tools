FROM alpine

ARG TARGETPLATFORM

RUN mkdir /tools && chmod 755 /tools
WORKDIR /tools

COPY .build/assets/${TARGETPLATFORM}/vscode-reh-web vscode-reh-web
COPY .build/assets/${TARGETPLATFORM}/gitlab-workflow-vscode-extension/extension vscode-reh-web/extensions/gitlab-workflow
COPY scripts/copy_tools.sh copy_tools.sh
COPY scripts/init_tools.sh init_tools.sh

ENTRYPOINT ["./copy_tools.sh"]
